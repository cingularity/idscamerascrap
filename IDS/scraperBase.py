from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup 
from Base.ConnectBase import connectBase


class IDS(connectBase):
    def __init__(self, link='https://en.ids-imaging.com/store/products/cameras/ids-interface/usb-2.0-usb-3.0-usb-3.1-gen.-1-gige/show/all.html'):
        super().__init__(link)
        
    
  
    def product_link_dict(self):
        bsobj = self.connect()
        raw_links = bsobj.findAll('span', {'class': 'product-name'})
        name_link = [(link.get_text().strip(), link.find('a')['href']) for link in raw_links]
        sensors = bsobj.findAll('ul', {'class': 'camera-list-info'})
        sensor_name = [(sensor.findAll('li')[3].get_text().strip()) for sensor in sensors]
        
            #product_link.update({link.get_text(): link.find('a')['href']})       
        product_link_sensor = list(zip(name_link, sensor_name))
        return product_link_sensor
    
    
