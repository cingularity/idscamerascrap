#from urllib.request import urlopen
#from urllib.error import HTTPError
#from bs4 import BeautifulSoup
from miscl.textCleaner import TextCleaner
from Base.ConnectBase import connectBase

class ScrapeIDS(connectBase): 
    def __init__(self, link):
        super().__init__(link)
        
        
    def getData(self):
        scrapped_data = {}
        bsobj = self.connect()
        # get image link
        image_link = bsobj.findAll('p', {'class': 'product-image'})[0].find('img')['src']
        
        #get Product name
        product_name = bsobj.findAll('div', {'class': 'product-name'})[0].get_text().strip()
        
        # get product description
        #product_description = bsobj.findAll('div', {'class': 'product-shop'}) # do it later
        
        # get Data Tab
        data_table = bsobj.findAll('table', {'class': 'data-table'})[1].findAll('tr')
        try:
            description = bsobj.find('div', {'class': 'std'}).get_text().strip()
        except:
            description = "Not available"
        try:
            sheet_link = "https://en.ids-imaging.com" + bsobj.findAll('td', {'class': 'data'})[-1].li.a['href']
        except:
            sheet_link = 'No link available'
        data_dict = {}
        for rows in data_table:
            #list_data = map(rows.find('th').get_text().strip(), rows.find('td').get_text().strip())
            data_dict.update({TextCleaner(rows.find('th').get_text().strip()).removeAll(): TextCleaner(rows.find('td').get_text().strip()).removeAll()})
            
        scrapped_data.update({'image': image_link, 'Product': product_name, 'Data': data_dict, 'Description': description, 'Link to sheet': sheet_link})
        return scrapped_data
    
    def getImage(self):
        pass
        
        