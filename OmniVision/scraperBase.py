from Base.ConnectBase import connectBase


'''
The Omni vision website has divided thier Sensor products in 4 Category:
Categories are as follows:
Above 13 megapixels: 'http://www.ovt.com/image-sensors/above-13-megapixels/'
8-13 megapixels: 'http://www.ovt.com/image-sensors/8-13-megapixels/' 
2-5 megapixels: 'http://www.ovt.com/image-sensors/2-5-megapixels/'
1 megapixel and below: 'http://www.ovt.com/image-sensors/1-megapixel-and-below/'

'''

class OmniVision(connectBase):
    def __init__(self, link):
        super().__init__(link)
        
    
    def link_creator(self, url):
        return 'http://www.ovt.com' + url
    
    def get_product_link(self):
        product_link = {}
        bsobj = self.connect()
        products = bsobj.findAll('div', {'class': 'product-sensor'})
        for product in products:
            link = self.link_creator(product.a['href'])
            name = product.a.get_text().strip('arrow_forward')
            product_link.update({name: link})
            
        return product_link