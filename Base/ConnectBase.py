from urllib.request import urlopen, HTTPCookieProcessor, build_opener
from urllib.error import HTTPError
from bs4 import BeautifulSoup

class connectBase(object):
    def __init__(self, link):
        self.link = link
        
    def connect(self):   
        try:
            html = urlopen(self.link)
        except HTTPError as e:
            print("error", e)
            return None
        
        try:
            bsobj = BeautifulSoup(html.read(), 'html.parser')
            return bsobj
        except AttributeError as e:
            print("error", e)
            return None