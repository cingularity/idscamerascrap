from urllib.request import urlopen
from urllib.error import HTTPError
from bs4 import BeautifulSoup 
from selenium import webdriver
from Base.ConnectBase import connectBase
#import re
from miscl.textCleaner import TextCleaner


'''
Usage:
avt = AVT()     #initialize AVT() # a default link for initialization is set. Do not change until you know
                # what you are doing.
                
category = avt.category_link_dict()        # get the links of the listing of categories
product_links = avt.get_product_url_from_category_url(category)    # the dictionary created above (avt.category_link_dict()) is fed
                                                                   # the function returns a dictionary with name of camera as the key
                                                                   # and link to the camera as the value





'''


class AVT(connectBase):
    def __init__(self, link='https://www.alliedvision.com/de/produkte/kameras/kameras-erweitertes-portfolio.html'):
        super().__init__(link)
        
        
        
    def link_creator(self, url):
        return 'https://www.alliedvision.com' + url
        
    
    def category_link_dict(self):
        category_link = {}
        bsobj = self.connect()
        raw_links = bsobj.findAll('div', {'class': 'av_prod_category_item'})
        for category in raw_links:
            category_link.update({TextCleaner(category.find('h2').getText().strip()).removeAll() : self.link_creator(category.find('a')['href'])})
        return category_link
        
        
    def source_with_phantom(self, url):        
        driver = webdriver.PhantomJS()
        driver.set_window_size(1120, 550)
        driver.get(url)
        source = BeautifulSoup(driver.page_source, 'html.parser')
        driver.quit()
        return source
    
    def get_product_url_form_category_url(self, links_dict):
        product_url_dict = {}
        for name, link in links_dict.items():
            source = self.source_with_phantom(link)
            # get the divs with link for product
            for page in source.findAll('div', {'data-ng-repeat': 'camera in group'}):
                product_url_dict.update({TextCleaner(page.find('a').getText().strip()).removeAll(): self.link_creator(page.find('a')['href'])})
        # return the dictionary created with product name and the link to it        
        return product_url_dict
        