from Base.ConnectBase import connectBase


'''
##for surface sensors use ##
link = 'https://www.framos.com/de/sensoren/flaechensensoren/'
## for Zeilensensoren ##
link = 'https://www.framos.com/de/sensoren/zeilensensoren/'
## for Sonsorsockel use ##
link = 'https://www.framos.com/de/sensoren/sensorensockel/'
'''
class FRAMOS(connectBase):
    def __init__(self, link='https://www.framos.com/de/sensoren/flaechensensoren/'):
        super().__init__(link)
        
        
    def link_creator(self, url):
        return 'https://www.framos.com' + url
    
    
    def next_last(self): 
        ''' This will return the link to next and last page '''
        links = []
        bsobj = self.connect()
        section = bsobj.findAll('div',{'class':'listing--paging'})[0].findAll('a')
        for link in section:
            try:
                links.append(self.link_creator(link['href']))
            except:
                pass
        return links
    
    
    def get_product_link(self):
        product_link = {}
        bsobj = self.connect()
        # section with relevant data only
        product_section = bsobj.findAll('div',{'class': 'listing'})[0].findAll('div', {'class': 'product--info'})
        for raw in product_section:
            for links in raw.findAll('a', {'class': 'product--title'}):
                try:
                    #print(links['title'], links['href'])
                    product_link.update({links['title']: links['href']})
                except:
                    pass
        return product_link